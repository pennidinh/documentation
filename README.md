

![](https://drive.google.com/uc?id=1egUKy9zmvJZqLoExIgsKL0AEIBgqb_4e)

#### Terms:

| Term            | Definition                                                   |
| --------------- | ------------------------------------------------------------ |
| PenniDinh Hub   | The client computer instance running PenniDinh Hub software typically running within the user's network. |
| Cloud PenniDinh | The components of PenniDinh that run in the cloud. For example, the API Gateways and Lambdas used by PenniDinh Hub's to update their PenniDinh SubDomains' IP address. |

### Setting up a new Shared PenniDinh Proxy Server:

https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/user-data.html

Create a new EC2 host with the following cloud-init cloud-config:

(replace shared.pennidinh.com with proxy domain name)

```
#cloud-config
repo_update: true
repo_upgrade: all

packages:
 - docker
 - git

runcmd:
 - [systemctl, start, docker]
 - [systemctl, enable, docker]
 - [ sh, -c, "curl -L https://github.com/docker/compose/releases/download/1.27.4/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose" ]
 - [sh, -c, "chmod +x /usr/local/bin/docker-compose"]
 - [git, config, --global, credential.helper, '!aws codecommit credential-helper $@']
 - [git, config, --global, credential.UseHttpPath, true]
 - [git, clone, "https://penniman26@bitbucket.org/pennidinh/pennidinhsharedproxy.git"]
 - [sh, -c, "echo -n PENNIDINH_CLIENT_ID= >> pennidinhsharedproxy/.env"]
 - [sh, -c, "echo `uuidgen` >> pennidinhsharedproxy/.env"]
 - [sh, -c, "echo 'EXTERNAL_DOMAIN_NAME=shared.pennidinh.com' >> pennidinhsharedproxy/.env"]
 - [sh, -c, "cd pennidinhsharedproxy/ && ./updateAndRestart.sh"]
 - [sh, -c, "(crontab -l 2>/dev/null; echo '@reboot /pennidinhsharedproxy/updateAndStart.sh') | crontab -"]
 - [sh, -c, "(crontab -l 2>/dev/null; echo '0 * * * * /pennidinhsharedproxy/updateAndStart.sh') | crontab -"]

``` 

After bootup, you can see the logs for the above cloud-init exeuction:

```
cat /var/log/cloud-init-output.log
```

### Preparing a PenniDinh Hub for shipment:

- install and startup docker

  - ```bash
    curl -sSL https://get.docker.com | sh
    sudo usermod -aG docker pi
    # reboot your pi so that you log back in with the updated permissions.
    # test step
    docker run hello-world
    ```

- install docker-compose

  - ```bash
    sudo pip3 -v install docker-compose
    ```

- clone this git repo https://bitbucket.org/pennidinh/pennidinhhomehub/src/master/

  - ```bash
    git clone https://penniman26@bitbucket.org/pennidinh/pennidinhhomehub.git
    ```

- create a file ".env" with the contents "PENNIDINH_CLIENT_ID=#{some new UUID}" (ex. "PENNIDINH_CLIENT_ID=0dbf4108-66a6-4135-87e1-7c321735611b")

  - ```bash
    sudo apt-get install uuid-runtime
    uuid=$(uuidgen)
    echo "PENNIDINH_CLIENT_ID=$uuid" > /home/pi/pennidinhhomehub/.env
    # confirm and get your client ID
    cat /home/pi/pennidinhhomehub/.env
    ```

- From the Ops Tools package, run "./assign-subdomain.sh" following the prompts for the above client ID and the pennidinh.com subdomain which you'd like to assign

- setup a cron task to invoke https://bitbucket.org/pennidinh/pennidinhhomehub/src/master/updateAndStart.sh upon reboot and every hour

  - ```bash
    crontab -e
    # editor opens. then add the below lines:
    
    @reboot /home/pi/pennidinhhomehub/updateAndStart.sh
    0 * * * * /home/pi/pennidinhhomehub/updateAndStart.sh
    ```

- setup a cron task to invoke every hour (at the half hour) so that it can be remote rebooted in case of an unrecoverable issue that needs manual intervention

  - ```bash
    sudo crontab -e
    # editor opens. then add the below lines:
    
    30 * * * * /home/pi/pennidinhhomehub/rebootIfNeeded.sh
    ```

- run "./updateAndStart.sh"

- The SubDomain Ownership Daemon will create an RSA public/private key-pair in the /keys/ directory. Cat out the public key contents. From the Ops Tools package, run "./assign-rsa-public-key.sh" following the prompts for the Client ID and the RSA public key contents.

  - ```bash
    cat /keys/public.pem
    ```

- The SubDomain Ownership Daemon will then request that Cloud PenniDinh generate an SSL cert for the hub's subdomains. Once the SSL cert is generated, the SubDomain Ownership Daemon will persist the SSL cert to /certs/. Confirm the certs are present in /certs/.

- The new PenniDinh Hub is now prepared.

### Components

#### Ops Tools

https://bitbucket.org/pennidinh/opstools/src/master/

Collection of CLI operations scripts typically written in ruby. (ex. assigning ClientIDs to PenniDinh Subdomains, assigning RSA public keys for Client IDs, and assigning shared proxy hosts and port numbers with some validation). 

#### Cloudformation

https://bitbucket.org/pennidinh/pennidinhcloudformation/src/master/

Definitions of AWS cloud components for version controlled cloud infrastructure. Currently, only the "Diagnostic" components are defined here.

#### Website

https://sites.google.com/s/1wKXEX-IvNHUAYZJqSJFsyZyNhXfkqZKO/p/0B9KE63Jtig7bQTNvNkd1UXdOdVU/edit

https://www.pennidinh.com/

Built using Google Sites

#### Documentation

https://bitbucket.org/pennidinh/documentation/src/master/

This git package. Documentation artifacts.

#### Mobile App

https://bitbucket.org/pennidinh/pennidinhexpoapp/src/master/

https://expo.io/@pennia/pennidinh

iOS https://testflight.apple.com/join/ASe23BVl

Android https://drive.google.com/open?id=1gL281mJ2ZfKqDV4TFsNl1E4E_-ouD71L 

The mobile app is built using Expo. The app is written using the React Native framework in javascript. The entry point React Native component is in https://bitbucket.org/pennidinh/pennidinhexpoapp/src/master/App.js. 

#### PenniDinh Hub

https://bitbucket.org/pennidinh/pennidinhhomehub/src/master/

Docker-compose configuration and startup/update scripts. This git package is what is cloned into the PenniDinh Hub and manages starting and updating all of the other components. 

##### Redis

https://bitbucket.org/pennidinh/pennidinhhomehub-redis/src

Redis server docker container. Redis is the primary local database used by the PenniDinh Hub.

##### NodeJS Web Server

https://bitbucket.org/pennidinh/pennidinhhomehub-nodejsserver/src

NodeJS server docker container. Primarily serves traffic proxied from the Apache Web Server.

##### Subdomain Ownership Daemon

(note, this is also used by the Cloud PenniDinh Shared Proxy)

https://bitbucket.org/pennidinh/pennidinhhomehub-subdomainownershipdaemon/src/master/

NodeJS Subdomain Ownership Daemon docker container. Using Hub's RSA private key, instructs the Cloud PenniDinh component to set/update IP addresses and to generate/fetch new SSL certs.

##### Apache Web Server

https://bitbucket.org/pennidinh/pennidinhhomehub-apacheserver/src/master/

Apache Web Server docker container. Internet facing entrance for PenniDinh Hub HTTP traffic.

\* https://bitbucket.org/pennidinh/pennidinhhomehub-apacheserver-config/src/master/

Apache Web Server configuration directory. Submodule of Apache Web Server docker container repository above

\* https://bitbucket.org/pennidinh/pennidinhhomehub-apacheserver-mod_auth_openidc_lwa_fork/src/master/

Apache Web Server mod_auth_openidc fork. Submodule of Apache Web Server docker container repository above. 1) The Amazon OAuth token size was too big and required a code change to increase the limit. 2) The OAuth inspection endpoint is validated to be an https endpoint; removed that validation to allow using a local OAuth inspection endpoint using a docker DNS name (i.e. "http://nodejsserver/oauth-inspection.js" ) over a docker-created local network.

\* https://bitbucket.org/pennidinh/pennidinhhomehub-apacheserver-documentroot/src/master/

Apache Web Server document root. Submodule of Apache Web Server docker container repository above.

##### Disk Space Maid

https://bitbucket.org/pennidinh/pennidinhhomehub-diskspacemaid/src

Ruby disk space maid docker container. Deletes oldest FTP upload files when the disk space is low.

##### Thumbnail Processor/Generator

https://bitbucket.org/pennidinh/pennidinhhomehub-thumbnailprocessor/src/master/

Ruby thumbnail processor daemon docker container. Pulls off the names of media files which need thumbnails from a redis queue, fetches the file from a shared file directory, and generates the thumbnail in that shared file directory. Repeats.

##### Thumbnail Sweeper

https://bitbucket.org/pennidinh/pennidinhhomehub-thumbnailsweeper/src/master/

Ruby thumbnail sweeper daemon docker container. Shouldn't be needed in the success case. Scans for media files which don't already have a thumbnail.

##### SSH Remote Proxy Daemon

https://bitbucket.org/pennidinh/pennidinhhomehub-proxydaemon/src/master/

Ruby ssh proxy daemon docker container. Generates an SSH public/private key-pair. Using RSA private key, uploads SSH public key to Cloud PenniDinh. Fetches assigned Cloud PenniDinh Shared Proxy hostname and port. Starts ssh remote proxy connection. This docker container is run in multiple instances once each for Apache Web Server port 443 and once for the debug SSH server (if enabled).

##### FTP Server

https://bitbucket.org/pennidinh/pennidinhhomehub-ftpdaemon/src/master/

Bash FTP daemon docker container. Fetches the FTP password from redis. If it exists, creates / updates a linux user with that password and starts up / restarts the FTP daemon. If password doesn't exist or no longer exists in redis, stops the FTP daemon. Periodically checks redis and repeats.

##### VPN Server

https://bitbucket.org/pennidinh/pennidinhhomehub-openvpn/src/master/

Ruby OpenVPN server deamon docker container. If public/private keypair does not exist, generates a key pair in persisted file directory. Starts / restarts OpenVPN server using keys and deny-list in persisted file directory. Queries redis for any entries in "users-to-add" or "users-to-remove" queues. If any entries exist, either create a VPN user's private key using the server's private key and create a VPN user config file with VPN users's private key and server's public key embedded. Persist VPN user config file to file directory shared with Apache Web Server. 

##### File Listener

https://bitbucket.org/pennidinh/pennidinhhomehub-filelistener/src/master/

Bash file listener daemon docker container. When a file is created, notifies the NodeJS server to update it's SQLite file index and, if its a media file, to push the filename onto a redis queue for the Thumbnail Processor Daemon to pick up. When a file is deleted, notifies the NodeJS server to remove the file from the SQLite file index.

##### (Debug) SSH Server

https://bitbucket.org/pennidinh/pennidinhhomehub-sshdaemon/src/master/

SSH server daemon docker container. Starts up an SSH server with a hardcoded username and password. This port is not accessible outside of other docker containers on the docker-created network on the PenniDinh Hub. This port is accessible through the SSH remote proxy daemon. This is helpful for accessing a PenniDinh Hub remotely without router port forwarding setup.

#### Cloud PenniDinh

##### Should Reboot APIs

https://bitbucket.org/pennidinh/pennidinhadminserver-shouldreboot/src

APIs for PenniDinh hubs to ask whether or not they need to reboot. Helpful for when a bug is pushed out to the updateAndStart.sh script and a reboot is required to restart the debug SSH remote proxy for regaining remote access to the hub. Ideally this would never have to be used to reboot a hub.

##### Diagnostic Report APIs

https://bitbucket.org/pennidinh/pennidinhadminserver-diagnostics/src

APIs for PenniDinh hubs to report their diagnostic information

##### Shared Proxy Management APIs

https://bitbucket.org/pennidinh/pennidinhadminserver-proxy/src/master/

- https://proxysockets.pennidinh.com/getProxyEndpoint?protocol=https&externalHostName=lynnwood.pennidinh.com
  * Access: public
  * Used by shared proxy web UI and the mobile app for determining the shared proxy host and port to redirect to / use
- https://proxysockets.pennidinh.com/uploadPubKey
  * Access: ClientID private key encrypted message
  * Used by Home Hub to upload client ssh public key. Used by the Cloud PenniDinh Shared Proxy to upload its server ssh public key.
- https://proxysockets.pennidinh.com/fetchPubKey
  * Access: public
  * Used by Home Hubs to fetch the shared proxy public key to ensure they are connecting to the shared proxy and not a man-in-the-middle
* https://proxysockets.pennidinh.com/getSharedProxyAsignees

##### Certificate Generator

https://bitbucket.org/pennidinh/pennidinhcertificategeneratoradmindaemon/src/master/ (private because google cloud keys are currently kept in version control :( )

NodeJS Certificate Generator Daemon docker container. Currently running on my own computer. Daemon which pulls from SQS queue for subdomains which need a new SSL cert. Generates the SSL cert and uploads the cert to the certs DynamoDB table.

##### Subdomain Management APIs

https://bitbucket.org/pennidinh/pennidinhadminserver-subdomainownership/src/master/

AWS Lambda fronted by API Gateway. Used by the PenniDinh Hub's Subdomain Ownership Daemon to update the hub's subdomain's DNS IP address and for generating and fetching the hub's subdomain's SSL cert. If a non-expired SSL cert does not exist, this lambda puts a message in an SQS queue instructing the Certificate Generator Daemon to create one.

- https://subdomainownership.pennidinh.com w/ encrypted payload that has list of subdomains and no dnsSettings structure - returns SSL cert for subdomain if it exists and isn't expired. otherwise adds subdomains to SQS queue.
- https://subdomainownership.pennidinh.com w/ encrypted payload that has list of subdomains and has dnsSettings structure - instructs DNS management service to update IP addresses pennidinh subdomains.
- https://subdomainownership.pennidinh.com otherwise, returns list of owned subdomains. 

##### Subdomain Lookup APIs

https://bitbucket.org/pennidinh/pennidinhadminserver-subdomainlookup/src/master/

AWS Lambda fronted by API Gateway. Simple function which returns whether or not the inputted subdomain name is one which is currently assigned to a PenniDinh Hub. Used for input form validation.

- (public) https://subdomainlookup.pennidinh.com/exists?subdomainToLookup=johndoe.pennidinh.com (this link works)

##### User authentication and authorization APIs

![](https://www.websequencediagrams.com/files/render?link=X4PvBiIVlahnj3kSBioPmQVEEtYsM7OcJd6carvv642SZ1Gex64Fg5vhoif4ZD9E)

source: [websequencediagrams](https://www.websequencediagrams.com/?lz=dGl0bGUgUGVubmlEaW5oIFVzZXIgQXV0aGVudGljYXRpb24gdy8gQnJvd3NlcgoKVXNlcidzAAgILT4AMApIdWI6ADkGbmF2aWdhdGVzIHRvIC9Mb2dpbiBVUkwgb24gdGhlaXIAZAtIdWIgdy8gUmVkaXJlY3RUbyBwYXJhbWV0ZXIKAE4NLT4AbA46ADYPcgA7B3MgdQCBHAZiAIEsBiB0byBDbG91ZACBWQtBUEkAgTcRABQQKGF1dGgucGVubmlkaW5oLmNvbSk6CgACJACBGREAbxAAgQ8cUHJvdmlkZXIgAIIwBihleC4ABAd3aXRoIEFtYXpvbikAgnURM1AAgyQQAEAJAD4FADEHOgoAAicAKCoAhCsGYQCEJwplcwCBJQZwAIFDBwBJKgCDUg8AggIJAINCGWJhY2sAg0wUAIImBWF1dGhvcml6AIU9BmNvZGUAgxddAIIHKS0AgQcSXG4tAIZ3CgCFJwZjbGllbnQgc2VjcmV0AIMMKgCFISUgLWFjY2VzcyB0b2tlblxuAAINIGV4cGlyAIdyBnRpbWVcbi1yZWZyZXNoACkGAIVcJwCGKiVFbmNyeXB0cyB0aGUgZm9sbG93aW5nIGludG8gYSAiAIIEEACIQwZUb2tlbiI6AIInDUh1YgCCKghJRACBQg8AgVEPXG4tAIFDH1xuAIMeBQCJdQsAhUMIIG5hbWUAg0BQRXhjaGFuZ2VzIACCeg1mb3IgcHJvZmlsZSBpbmZvcm0AiwYGKGkuZS4gZW1haWwgYWRkcmVzcykAg1BQAE8pAIM-Tm9kZQCDaSJIdWIAg24bAIQYEQCDWhUAgRFPAItIJycAjRUGAIktEgCNfRF0aGVzZSBxdWVyeQCOCQZzAIVdEgCBVg4AjjYKAI8CHwCORRAAjzAPTWFwcyBhIFVVSUQAglobIElEIiB0bwCCexwuXG4ARgYAiEoHaW5nAEEVQQCJBAYAh08GADYeIElEIi5cblZhbGlkAJBYBQCQKgtpcyBlaXRoZXIgMSkgYSByZWxhdGl2ZQCQcAZyIDIpIGFuAIwJCWVkAJEJBWZvciBtb2JpbGUgYXBwIGRlZXAtbGlua2luZwCQXB9TZQCJLQcAgTAdYXMgYSBjb29raWUuXG4AkVoIAIlnBnUAkRMHIgCRbwoiAJI2JXNlbmRzIEhUVFAgcmVxdWVzdACQFAVpAJJVBSAvc2VjdXJlIHNjb3BlAINNHi1GaW5kcwCDRSAAkHAFAIsCB0kAgx8UXG4AKCEAOA4AhDgXXG4tRXh0cmFjdHMgb3V0AIlTDiBmcm9tAId1EACELAcAih0HLlxuLUNvbmZpcm1zAC4PaXMgAIIYByJhbGxvd2VkRW1haWxBAIo3BmVzIiBzZXQuXG4tU2VydmVzAIJQCACUYB8AgnYNcmVzcG9uc2UK&s=rose)

https://bitbucket.org/pennidinh/pennidinhadminserver-auth/src/master/

AWS Lambda fronted by API Gateway. Used for logging in users and getting information such as email address used for email-address-based access control.

TODO profile and expiration time are encrypted using hub's public key which could be done by a malicious actor allowing them to login for a particular user. PenniDinh hub needs to fetch the profile and expiration time / or verify information with cloud PenniDinh.

- https://auth.pennidinh.com/ - used by PenniDinh Hub's for use login. PenniDinh Hub will redirect user's browser to this endpoint. PenniDinh Hub ClientID, RedirectToUrl, isBrowserSessionOnly, and Provider query params must be provided. Encodes those values into the OAuth "state" param and redirects to one of the OAuth providers supported (Amazon, Google, and Facebook).
- https://auth.pennidinh.com/?acceptAuthTokens=true - URL which provider will redirect to after a user succesfully authenticates and grants access to PenniDinh. This lambda will exchange the provider's one-time authorization code for an access and refresh token. Then this lambda will fetch the profile information (such as email address for authorization checks in the PenniDinh Hub) using the access token. Then this information will be redirected back to the "RedirectToUrl" originally specificed by the incoming request (above). This information is encrypted using the hub's RSA public key such that only the hub will be able to extract any of this information.
- https://auth.pennidinh.com/?pennidinhToken=#{token} - used by the PenniDinh Hub's OAuth token inspectection endpoint to . When the PenniDinh Hub's apache web server recieves a request for a URL which requires auth, the OAuth token in the request if forwarded to the OAuth inspection URL which is configured to be an API in the NodeJS PenniDinh Hub server. That OAuth token is encrypted using the PenniDinh Hub's RSA public key. The OAuth token is decrypted by the PenniDinh Hub. If the expiration time in the decrypted message is expired, then the PenniDinh hub reaches out to this endpoint with the PenniDinh Cloud encrypted token to get the latest profile information (such as email address) and to confirm that the OAuth access is granted. This lambda will decrypt the PenniDinh Cloud encrypted token, extract out the provider OAuth refresh token, the provider name, then exchange the OAuth refresh token with a new access token, encrypt a new PenniDinh Cloud token with the updated access token and original refresh token, and finally encrypt a new PenniDinh Hub token with the PenniDinh Cloud token, new expiration time, and new profile information.

##### Shared Proxy

https://bitbucket.org/pennidinh/pennidinhsharedproxy/src/master/

Docker-compose configuration and startup/update scripts.

###### SSH Server

https://bitbucket.org/pennidinh/pennidinhsharedproxy-sshusers/src/master/

Ruby ssh remote proxy server daemon docker container. Fetches all of the assigned hubs to the shared proxy instance with their assigned port number, fetches the ssh public keys of the assigned hubs, creates linux users for each of the assigned hubs, generates an SSH daemon config specifying each assigned hub as a linux user with restricted access to only allow remote proxy SSH connections restricted to just their assigned port, starts up / restarts ssh server. Periodically repeats.

###### Apache Web Server

https://bitbucket.org/pennidinh/pennidinhsharedproxy-apache/src/master/

Apache Web Server daemon docker container. Static configurations. Proxies to internal port based on URL query param OR HTTP cookie. Periodically, pulls "reboot-apache" value from redis and reboots apache if that value has changed. This would typically be used for when a new certificate is fetched by the subdomain ownership module.

###### Subdomain Ownership 

(see Subdomain Ownership from PenniDinh Hub section)

### Security

#### Shared Proxy

The shared proxy is a major security vulnerability if not properly secured. Access to the shared proxy must be carefully protected. The PenniDinh Hub currently reverse proxies two types of traffic from the shared proxy host 1) HTTPS traffic and 2) SSH traffic. The SSH access must be particularly protected because it doesn't have the same authentication and authorization checks that the PenniDinh HTTPS web server has; the SSH server has a default password (TODO generate SSH password on device and upload that SSH password to PenniDInh cloud.). The Shared Proxy host firewall only allows incoming requests on ports 80 and 443 (HTTPS) and 22 (SSH). The SSH access is protected with an SSH asymetic public/private key which only the PenniDinh Cloud admin has access to. The HTTPS endpoint allows the user to enter their subdomain into a basic web UI form, request that the PenniDinh Cloud Proxy Server sets a specific "proxy port" cookie, and (once that cookie is set) proxies the web traffic to that specific internal port.

###### Mock PenniDinh Hub man-in-the-middle

It is important that Cloud PenniDinh Proxy only proxies to the assigned PenniDinh Hub based on the "proxy port" cookie. To guarentee this, we must guarentee that the assigned proxy port is not being used by another PenniDinh Hub. This is guarentee is currently implemented by a daemon running on the Cloud PenniDinh Proxy which fetches all of the proxy assignments for that specfiic Shared Proxy from the Cloud PenniDinh table. For each proxy assignment, the Cloud PenniDinh Proxy daemon creates a linux user (using the PenniDinh Hub's domain name) and fetches the PenniDinh Home Hub Client ID's ssh public key (which has been uploaded by the PenniDinh Hub SSH client Docker container) and adds that public key to the linux user's ssh authorized_keys file [[ref](https://bitbucket.org/pennidinh/pennidinhsharedproxy-sshusers/src/d1119cd1f53e6f427b1a2cec6c582fa21f10d41e/fetchUsers.rb#lines-48:62)]. Then, again for each proxy assignment, the Cloud PenniDinh Proxy daemon sets an SSH daemon config rule such that the proxy assignment's linux user may only open a remote proxy on the specific assigned port and sets the "ForceCommand" to a dummy value that will exit such that the SSH client cannot do anything else except for create a remote proxy connection [[ref](https://bitbucket.org/pennidinh/pennidinhsharedproxy-sshusers/src/d1119cd1f53e6f427b1a2cec6c582fa21f10d41e/sshd_config.erb#lines-123:134)], and then restarts the SSH daemon. The Cloud PenniDinh Proxy daemon executes every 5 minutes.

###### Mock Cloud PenniDinh Proxy man-in-the-middle

Particularly for SSH traffic, its important that the SSH client creating the reverse proxy connection with the Cloud PenniDinh Proxy does not reverse proxy traffic from a malicous / mock Cloud PenniDinh Proxy endpoint. To guarentee this, the Cloud PenniDinh Proxy generates a SSH rsa private/public key pair and uploads the public key to the Cloud PenniDinh datastore. The PenniDinh Hub SSH client first fetches this public SSH key from an HTTPS endpoint and adds the SSH pub key to the "known hosts" [[ref](https://bitbucket.org/pennidinh/pennidinhhomehub-proxydaemon/src/7a287f14e082636a3887058f56580f68d67cf102/setup.sh#lines-126,153,154)] before making the SSH reverse proxy connection guarenteeing that the response from the SSH server can be decrypted with the known SSH public key.

###### Sharing a Hostname 

By sharing a hostname, there a new security vulnerabilities if not properly secured. Every login / session token cookie must be cleared before the the "proxy port" cookie can be set; otherwise, a login / session token from one PenniDinh Hub could be sent to another PenniDinh Hub upon changing the "proxy port", captured, and re-used maliciously on the original PenniDinh Hub. Cookies may not be set from another domain; they may only be set by an HTTP response served by that domain or by javascript which is included by an HTTP response served by that domain. The current PenniDinh Hub and the Shared Proxy are the only entities that can set the cookies or reference javascript which can set the cookies for the shared proxy domain. The current PenniDinh Hub is trusted to not be malicous against itself and only provides javascript files that are self-hosted / not referencing other domains. The API on the shared PenniDinh Hub which sets the "proxy port" cookie will clear all of the login / session cookies [[ref](https://bitbucket.org/pennidinh/pennidinhsharedproxy-apache/src/69110dd80e3fe3283b8e6c2d5329bb6cc210f4e2/apacheCgi/setProxyEndpoint.cgi#lines-20:24)]. The Shared Proxies basic UI web page only uses self-host / not referencing other domains javascript.

TODO seperate SSH and HTTPS proxy daemons on Cloud PenniDinh such that the SSH ports can't be accessed through apache?

#### FTP Daemon

FTP username and password information is not encrypted when sent from the camera to the PenniDinh Hub. The messages are usually only sent across the user's home network, but the risk of FTP username / password stealing exists and should be assumed to be compromised. The FTP daemon operates inside it's own Docker container with access to the shared FTP filesystem / directory and redis (for fetching the FTP user's current password). The FTP deamon Docker container creates a default "PenniDinh" user with the password specified by the user. *If the user hasn't specified a password or turned off the FTP daemon feature through the mobile or web app, the FTP daemon is shut down.* The FTP daemon is configured such that authenticated users may only write files (read access turned off). If the FTP username and password are stolen, the worst that an attacker could do is write a bunch of files. The Home Hub admin could then change the FTP password in the web or mobile app to mitigate the attack.

#### Home Hub -> Cloud PenniDinh Authentication and Authorization

##### Authenticating requests from the Home Hub

Initial Setup: An RSA asymetic key is generated on the hub when the PenniDinh Hub is first started. Through a trusted channel (currently done manually), the Client ID and RSA public key is copied from the hub and uploaded to the PenniDinh Cloud. 

The Home Hub has an RSA private key which encrypts a token "X-Pennidinh-Token" sent to the Cloud PenniDinh endpoints using standard encryption libraries. The Home Hub includes the hub's Client ID in a special HTTP header "X-Pennidinh-Clientid". The PenniDinh Cloud, aware of each client ID's public key, fetches the public key and decrypts the token. In each encrypted token body is a timestamp of when the message was sealed; the Cloud PenniDinh asserts that the current time is within 10 seconds of the sealed time. Also in each encrypted token body is an MD5 hash of the HTTP POST request's body; the PenniDinh Cloud hashes the request body and asserts that the MD5 hashes match.

##### Authoriizing requests from the Home Hub

The Cloud PenniDInh, aware of which ClientID has ownership of which PenniDinh subdomain, fetches the owning ClientID for each PenniDinh subdomain which the request requires access to and asserts that the Client ID in the request (which was used for decrypting the token) matches the owning Client ID from the subdomain ownership table.





TO BE DOCUMENTED

https://bitbucket.org/pennidinh/pennidinhadminserver-internalsharedproxyasignees/src/master/



https://bitbucket.org/pennidinh/pennidinhadminserver-certificateworker/src/master/



Cloudwatch Dashboard :https://cloudwatch.amazonaws.com/dashboard.html?dashboard=MainDashboard-1zYCSptCZrm4&context=eyJSIjoidXMtZWFzdC0xIiwiRCI6ImN3LWRiLTMzNDA5OTk0OTE3MyIsIlUiOiJ1cy1lYXN0LTFfY1NVTjZXcGtDIiwiQyI6IjNmZ2dzZGpzYnA2bG81ZGV0OGRiZ2UwczlvIiwiSSI6InVzLWVhc3QtMTpkNjgxZDRiYy1jY2Q3LTQ3MTQtYWM2Ni1hMzlkNDM3N2M4MjAiLCJPIjoiYXJuOmF3czppYW06OjMzNDA5OTk0OTE3Mzpyb2xlL3NlcnZpY2Utcm9sZS9DbG91ZFdhdGNoRGFzaGJvYXJkLVJlYWRPbmx5QWNjZXNzV2l0aExvZ3MtTWFpbkRhc2hib2FyLUpBM05RUElKIiwiTSI6IlVzclB3U2luZ2xlIn0=
